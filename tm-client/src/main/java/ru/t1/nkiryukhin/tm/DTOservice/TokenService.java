package ru.t1.nkiryukhin.tm.DTOservice;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.service.ITokenService;

@Getter
@Setter
@Component
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
