package ru.t1.nkiryukhin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.request.ProjectClearRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CLEAR]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

}
