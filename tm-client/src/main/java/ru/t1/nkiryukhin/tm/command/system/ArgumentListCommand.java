package ru.t1.nkiryukhin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.model.ICommand;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show argument list";
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
