package ru.t1.nkiryukhin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.nkiryukhin.tm.dto.response.TaskListByProjectIdResponse;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.util.Collections;
import java.util.List;

@Component
public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "list task by project id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @NotNull final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        @Nullable List<TaskDTO> tasks = response.getTasks();
        if (tasks == null) tasks = Collections.emptyList();
        renderTasks(tasks);
    }

}
