package ru.t1.nkiryukhin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(id);
        getTaskEndpoint().removeTaskById(request);
    }

}
