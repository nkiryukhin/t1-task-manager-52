package ru.t1.nkiryukhin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.DTOservice.CommandService;
import ru.t1.nkiryukhin.tm.DTOservice.LoggerService;
import ru.t1.nkiryukhin.tm.DTOservice.PropertyService;
import ru.t1.nkiryukhin.tm.DTOservice.TokenService;
import ru.t1.nkiryukhin.tm.api.endpoint.*;
import ru.t1.nkiryukhin.tm.api.repository.ICommandRepository;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nkiryukhin.tm.exception.system.CommandNotSupportedException;
import ru.t1.nkiryukhin.tm.repository.CommandRepository;
import ru.t1.nkiryukhin.tm.util.SystemUtil;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.nkiryukhin.tm.command";

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IAdminEndpoint adminEndpoint;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareStartup() throws AbstractException {
        initPID();
        initCommands(abstractCommands);
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    void processCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private boolean processArguments(@Nullable final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(@Nullable final String argument) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void initCommands(@NotNull final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) continue;
            command.setServiceLocator(this);
            commandService.add(command);
        }
    }

    public void run(@Nullable final String[] args) {
        try {
            processArguments(args);
            prepareStartup();
        } catch (@NotNull final AbstractException e) {
            loggerService.error(e);
        }
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}