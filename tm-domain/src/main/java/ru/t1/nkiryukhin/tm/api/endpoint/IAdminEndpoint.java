package ru.t1.nkiryukhin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseCreateRequest;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseDropRequest;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseCreateResponse;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseDropResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AdminEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAdminEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAdminEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAdminEndpoint.class);
    }

    @NotNull
    @WebMethod
    DatabaseCreateResponse createDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseCreateRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    DatabaseDropResponse dropDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseDropRequest request
    ) throws Exception;

}
