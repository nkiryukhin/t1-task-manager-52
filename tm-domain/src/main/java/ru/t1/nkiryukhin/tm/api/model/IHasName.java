package ru.t1.nkiryukhin.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
