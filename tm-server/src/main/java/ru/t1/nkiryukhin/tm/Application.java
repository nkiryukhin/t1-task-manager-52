package ru.t1.nkiryukhin.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}