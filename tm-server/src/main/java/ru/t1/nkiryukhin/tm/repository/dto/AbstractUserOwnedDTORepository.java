package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.nkiryukhin.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    public AbstractUserOwnedDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}