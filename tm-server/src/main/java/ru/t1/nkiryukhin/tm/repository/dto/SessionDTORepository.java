package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class SessionDTORepository extends AbstractDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class).setParameter("userId", userId).getResultList();
    }

}
